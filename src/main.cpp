#include <Arduino.h>
// support for the wifi on the feather
#include <SPI.h>
#include <WiFi101.h>
#include <WiFiUdp.h>
// supports emulated flash storage on the M0
//#include <FlashAsEEPROM.h>
#include <JC_Button.h>
//#include <OSC .h>
#include <jled.h>
#include <Adafruit_ZeroTimer.h>
// some constants
#include "secrets.h"
#include <OSCMessage.h>

// setup a timer to make the leds behave
Adafruit_ZeroTimer zerotimer = Adafruit_ZeroTimer(3);

// setup the timer (100 HZ)
uint8_t divider  = 16;
uint16_t compare = 30000;
tc_clock_prescaler prescaler = TC_CLOCK_PRESCALER_DIV16;



int status = WL_IDLE_STATUS;     // the WiFi radio's status

//setup the LEDS\s
const int BrightLevel = 30;
// this is pin 11 b/c there is something very strange about pin
// 10...trying to use it results in a crash!?!?
auto PowerStatus = JLed(11).Off().MaxBrightness(BrightLevel);
auto WifiStatus = JLed(6).Breathe(500).DelayAfter(100).Forever().MaxBrightness(BrightLevel);
auto ShowStatus = JLed(5).Off().MaxBrightness(BrightLevel);

// osc stuff
WiFiUDP Udp;                                // A UDP instance to let us send and receive packets over UDP
const unsigned int outPort = 12321;          // remote port to receive OSC
const unsigned int localPort = 8888;        // local port to listen for OSC packets (actually not used for sending)



Button FwdButton(13);
Button BackButton(12);
Button ShowButton(10);


#define VBATPIN A7


WiFiClient client;

// Establish a OSC client
//OSCClient client;

// Create new OSC message
//OSCMessage oscMessage;

void TC3_Handler() {
  Adafruit_ZeroTimer::timerHandler(3);
}

void TimerCallback0(void)
{
  WifiStatus.Update();
  PowerStatus.Update();
  ShowStatus.Update();
}

void blinkStatus() {
  ShowStatus.Blink(200,100);
}
void connectWifi() {
  status = WiFi.status();
  if ( status != WL_CONNECTED) {
    // set the led back to fast blink
    WifiStatus.Breathe(500).DelayAfter(100).Forever();
  }
  while ( status != WL_CONNECTED) {
    status = WiFi.begin(ssid, pass);
  } 
  // Wifi is connected
  Serial.println("try to turn led to slow");
  WifiStatus.Breathe(2000).DelayAfter(500).Forever();

}

void connectServer() {
  while (!client.connect(server, 51234)) {
		delay(100);
    status = WiFi.status();
    // make sure we have WIFI
    if (status != WL_CONNECTED) {
      connectWifi();
    }
  }
  // server is connected
  WifiStatus.On();
}


void OscSend(const char message[]) {
  Serial.println(message);
  OSCMessage msg(message);
  //msg.add(3).add(1);
  Udp.beginPacket(server, outPort);
  msg.send(Udp);
  Udp.endPacket();
  msg.empty();
}

void httpRequest(const char pressBank[]) {

  // close any connection before send a new request.

  // This will free the socket on the WiFi shield

  client.stop();

  // if there's a successful connection:

  if (client.connect(server, 8000)) {
    Serial.println("connecting...");
    // send the HTTP GET request:
    client.println(String("GET ") + pressBank + " HTTP/1.1");
    client.println("Host: 192.168.1.167");
    client.println("User-Agent: ArduinoWiFi/1.1");
    client.println("Connection: close");
    client.println();
  }  else {
    // if you couldn't make a connection:
    WifiStatus.Breathe(2000).DelayAfter(500).Forever();
  }
}


void setup() {
  Serial.begin(9600);
  zerotimer.enable(false);
  zerotimer.configure(prescaler,       // prescaler
          TC_COUNTER_SIZE_16BIT,       // bit width of timer/counter
          TC_WAVE_GENERATION_MATCH_PWM // frequency or PWM mode
          );

  zerotimer.setCompare(0, compare);
  zerotimer.setCallback(true, TC_CALLBACK_CC_CHANNEL0, TimerCallback0);
  zerotimer.enable(true);
// put your setup code here, to run once:
  //Configure pins for Adafruit ATWINC1500 Feather
  WiFi.setPins(8,7,4,2);
  // attempt to conencto the wifi-network
  connectWifi();
  connectServer();

  Udp.begin(localPort);
  
  FwdButton.begin();
  BackButton.begin();
  ShowButton.begin();
  // low power mode turned off to see impact to responsivity
  // WiFi.lowPowerMode();
}

void loop() {
  // read all the buttons
  FwdButton.read();
  BackButton.read();
  ShowButton.read();
  //PowerStatus.Update();
  // read the input buffer (probably nothing in it)
  if (client.available()) {
    char c = client.read();
  }

  if (client.connected()) {

    if (FwdButton.wasPressed())
    {
      Serial.println("FWD Button");
      //client.println(button_one);
      httpRequest(button_one);
      blinkStatus();
    }
    if (BackButton.wasPressed())
    {
      Serial.println("Back Button");
      //client.println(button_two);
      httpRequest(button_two);
      blinkStatus();
    }
    if (ShowButton.wasPressed())
    {
      Serial.println("Show Button");
      //client.println(button_three);
      httpRequest(button_three);
      blinkStatus();
    }
  }
  else
  {
    // try to reconnect
    connectWifi();
    connectServer();
  }

  // check the battery voltage
  float measuredvbat = analogRead(VBATPIN);
  measuredvbat *= 2;    // we divided by 2, so multiply back
  measuredvbat *= 3.3;  // Multiply by 3.3V, our reference voltage
  measuredvbat /= 1024; // convert to voltage
  if (measuredvbat < 3.3) {
    // batterys dead make light steady
    PowerStatus.On();
  } else if (measuredvbat < 3.6) {
    // start the red led "breathing"
    PowerStatus.Breathe(500).DelayAfter(100).Forever();
  } else {
    // make sure its off
    PowerStatus.Off();
  }

}